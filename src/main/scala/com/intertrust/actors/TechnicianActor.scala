package com.intertrust.actors

import java.time.{Clock, Instant}

import akka.actor.{Actor, ActorRef}
import akka.persistence.{PersistentActor, RecoveryCompleted, SnapshotOffer}
import com.intertrust.actors.TechnicianActor.{NeedSendAlert, RecoveryState, SendAlertEvent, Snapshot, State}
import com.intertrust.behaviour.{Behaviour, ReplyBehaviour}
import com.intertrust.protocol.Movement.{Enter, Exit}
import com.intertrust.protocol._

class TechnicianMovementError(prev: TechnicianMovement, current: TechnicianMovement) {
  def text: String = s"unexpected movement from $prev to $current"
}

object TechnicianMovementError {
  def apply(prev: TechnicianMovement, current: TechnicianMovement): TechnicianMovementError = new TechnicianMovementError(prev, current)
}

object TechnicianActor {

  case object NeedSendAlert

  case class SendAlertEvent(alert: MovementAlert)

  case class State(movement: TechnicianMovement) {
    def update(movement: TechnicianMovement): State = {
      State(movement)
    }
  }

  case class RecoveryState(behaviour: Behaviour, lastSentAlert: Option[MovementAlert])

  case class Snapshot(behaviour: Behaviour, state: State)

}

class TechnicianActor(technicianID: String, clock: Clock, alertActor: ActorRef)
  extends Actor with PersistentActor {

  var state: State = State(TechnicianMovement(Vessel("0"), Exit))
  var recoveryState: RecoveryState = RecoveryState(OutdoorBehaviour(), None)
  var alert: Option[MovementAlert] = None

  def snapshotInterval: Int = 2000

  def persistenceId: String = s"technician-$technicianID"

  def updateState(movement: TechnicianMovement): Unit = {
    state = state.update(movement)
  }

  protected def processing(behaviour: ReplyBehaviour): Receive = new Receive {
    def apply(msg: Any): Unit = {
      alert match {
        case Some(a) =>
          alertActor ! a
          // if persist message is failed then two notification will be sent.
          // One of messages from code line below and other from recovery function
          // it is more safely then firstly persist message and then send alert
          persist(SendAlertEvent(a)) { _ => alert = None }
        case None =>
      }

      msg match {
        case e: TechnicianMovement =>
          persist(e) { e =>
            val (nextBehaviour, reply) = behaviour.applyWithReply(msg)

            context.become(processing(nextBehaviour match {
              case b: ReplyBehaviour => b
              case _ => throw new RuntimeException("Invalid behaviour")
            }))

            updateState(e)
            if (lastSequenceNr % snapshotInterval == 0 && lastSequenceNr != 0) {
              saveSnapshot(Snapshot(nextBehaviour, state))
            }

            sender() ! reply
          }
        case _ =>
      }
    }

    def isDefinedAt(x: Any): Boolean = true
  }

  def receiveCommand: Receive = processing(OutdoorBehaviour())

  case class IndoorBehavior() extends ReplyBehaviour {
    def applyWithReply(msg: Any): (Behaviour, Reply) = {
      msg match {
        case TechnicianMovement(location, Exit) if location.equals(state.movement.location) =>
          (OutdoorBehaviour(), GoodMovementReply)
        // ignore this case, because this situation can be appeared to due to retry
        case TechnicianMovement(location, Enter) if location.equals(state.movement.location) =>
          (IndoorBehavior(), GoodMovementReply)
        case current: TechnicianMovement =>
          (sendAlertAndResolveUnexpectedMovement(state.movement, current), BadMovementReply)
        case _ => (IndoorBehavior(), GoodMovementReply)
      }
    }
  }

  case class OutdoorBehaviour() extends ReplyBehaviour {
    def applyWithReply(msg: Any): (Behaviour, Reply) = {
      msg match {
        case TechnicianMovement(_, Enter) =>
          (IndoorBehavior(), GoodMovementReply)
        // ignore this case, because this situation can be appeared to due to retry
        case TechnicianMovement(location, Exit) if location.equals(state.movement.location) =>
          (OutdoorBehaviour(), GoodMovementReply)
        case current: TechnicianMovement =>
          (sendAlertAndResolveUnexpectedMovement(state.movement, current), BadMovementReply)
        case _ =>
          (OutdoorBehaviour(), GoodMovementReply)
      }
    }
  }

  protected def sendAlert(prev: TechnicianMovement, current: TechnicianMovement): Unit = {
    alert = Some(MovementAlert(Instant.now(clock), technicianID, TechnicianMovementError(prev, current).text))
    // Need send this messages otherwise there may not be a messages in mailbox and alerts will not be sent
    self ! NeedSendAlert
  }

  protected def sendAlertAndResolveUnexpectedMovement(prev: TechnicianMovement, current: TechnicianMovement): Behaviour = {
    sendAlert(prev, current)
    current match {
      case TechnicianMovement(_, Enter) =>
        IndoorBehavior()
      case TechnicianMovement(_, Exit) =>
        OutdoorBehaviour()
    }
  }

  def receiveRecover: Receive = {
    case SnapshotOffer(_, snapshot: Snapshot) =>
      state = snapshot.state
      recoveryState = RecoveryState(snapshot.behaviour, None)
    case e: TechnicianMovement =>
      recoveryState = RecoveryState(recoveryState.behaviour.apply(e), None)
      updateState(e)
    case SendAlertEvent(a) =>
      recoveryState = RecoveryState(recoveryState.behaviour, Some(a))
    case RecoveryCompleted =>
      (recoveryState.lastSentAlert, alert) match {
        case (Some(a1), Some(a2)) if a1.equals(a2) => alert = None
        case _ =>
      }

      context.become(processing(recoveryState.behaviour match {
        case b: ReplyBehaviour => b
        case _ => throw new RuntimeException("Invalid behaviour")
      }))
    case _ =>
  }
}
