package com.intertrust.actors

import java.time.Clock
import java.util.concurrent.locks.{ReadWriteLock, ReentrantReadWriteLock}

import enumeratum.{Enum, EnumEntry}
import akka.actor.{Actor, ActorRef, Cancellable, Timers}
import akka.persistence.{PersistentActor, RecoveryCompleted, SnapshotOffer}
import com.intertrust.behaviour.Behaviour
import com.intertrust.protocol.Movement.{Enter, Exit}
import com.intertrust.protocol.{Reply, TurbineAlert, TurbineStatus, TurbineTechnicianMovement}
import com.intertrust.protocol.TurbineStatus.{Broken, Working}
import com.intertrust.time.FiniteDurationConverter

import scala.concurrent.duration._
import scala.collection.immutable.{IndexedSeq, Set}
import scala.collection.mutable
import scala.concurrent.ExecutionContext

sealed abstract class TurbineError(errorText: String) extends EnumEntry {
  def text: String = errorText
}

object TurbineError extends Enum[TurbineError] {
  override val values: IndexedSeq[TurbineError] = findValues

  case object TurbineStopWorking extends TurbineError("turbine stops working")

  case object TurbineStillBrokenAfter4Hour extends TurbineError("turbine has been in a Broken state for more than 4 hours and no technician has entered the turbine yet to fix it")

  case object TurbineStillBrokenAfterRepair extends TurbineError("technician exits a turbine without having repaired the turbine; the condition here is that if the turbine continues in a Broken state for more than 3 minutes after a technician has exited the turbine")

}

object TurbineActor {

  case object NeedSendAlerts

  // Have to store String instead of object because serializer swears
  case class SendAlertEvent(errorText: String)

  case class State(technicians: Set[String], status: TurbineStatus) {
    def update(e: Any): State = {
      e match {
        case TurbineTechnicianMovement(id, Enter) => copy(technicians + id, status)
        case TurbineTechnicianMovement(id, Exit) => copy(technicians - id, status)
        case status: TurbineStatus => copy(technicians, status)
        case _ => this
      }
    }
  }

  case class RecoveryState(nextBehaviour: Behaviour, lastEventIsAlert: Option[String])

}

class TurbineActor(turbineID: String, durationConverter: FiniteDurationConverter, clock: Clock, alertActor: ActorRef)
  extends Actor with Timers with PersistentActor {

  import TurbineActor._
  import TurbineError._

  implicit val executeContext: ExecutionContext = context.system.dispatcher
  var state = State(Set.empty[String], Working)
  var lastSnapshotSequenceNr: Long = 0L
  var cancellable: Option[Cancellable] = None

  // alertsErrorQueue is used instead of send message through `timers.startSingleTimer` because
  // If use for this case `timers.startSingleTimer` (i.e send `SendAlertEvent` message to mailbox using timers.startSingleTimer
  // and after that to send alert to `AlertActor` when `receiveCommand` receive this message)
  // then there will be possability the following situation: timer will send message `SendAlertEvent` to mailbox,
  // but there may be Working, Broken events in the mailbox already. If actor is crashed on Broken event then `SendAlertEvent` is lost.

  // P.S Can't use priority mailbox because when PersistentActor is used mailbox should implement DequeBasedMessageQueueSemantics
  val alertsErrorQueue: mutable.Queue[TurbineError] = mutable.Queue.empty

  val lock: ReadWriteLock = new ReentrantReadWriteLock()
  var recoveryState = RecoveryState(WorkingBehaviour(), None)


  def updateRecoveryState(behaviour: Behaviour, lastSendAlertError: Option[String]): Unit = {
    recoveryState = recoveryState.copy(behaviour, lastSendAlertError)
  }

  def sendAlert(error: TurbineError): Unit = {
    synchronized {
      alertsErrorQueue += error
    }

    // Need send this messages otherwise there may not be a messages in mailbox and alerts will not be sent
    self ! NeedSendAlerts
  }

  def persistenceId: String = s"turbine-$turbineID"

  def needToMakeSnapshot(lastSequenceNr: Long): Boolean = lastSequenceNr - lastSnapshotSequenceNr > 2000

  def updateState(e: Any): Unit = {
    state = state.update(e)
  }

  case class WorkingBehaviour() extends Behaviour {
    def apply(msg: Any): Behaviour = {
      msg match {
        case Broken =>
          sendAlert(TurbineStopWorking)
          if (state.technicians.isEmpty) {
            startTimer(TurbineStillBrokenAfter4Hour, 4.hours)
            BrokenBehaviour()
          } else {
            TechnicianInBehavior()
          }
        case _ => WorkingBehaviour()
      }
    }
  }

  case class BrokenBehaviour() extends Behaviour {
    def apply(msg: Any): Behaviour = {
      msg match {
        case TurbineTechnicianMovement(_, Enter) =>
          stopTimer()
          TechnicianInBehavior()
        case Working =>
          stopTimer()
          WorkingBehaviour()
        case _ => BrokenBehaviour()
      }
    }
  }

  case class TechnicianInBehavior() extends Behaviour {
    def apply(msg: Any): Behaviour = {
      msg match {
        case TurbineTechnicianMovement(_, Exit) =>
          if (state.technicians.isEmpty) {
            startTimer(TurbineStillBrokenAfterRepair, 3.minutes)
            BrokenBehaviour()
          } else {
            TechnicianInBehavior()
          }
        case Working => WorkingBehaviour()
        case _ => TechnicianInBehavior()
      }
    }
  }

  protected def processingBehaviour(behaviour: Behaviour): Receive = new Receive {
    def apply(msg: Any): Unit = {
      val cSender = context.sender()

      val events = synchronized {
        val events = for (
          err <- alertsErrorQueue
        ) yield (SendAlertEvent(err.text), TurbineAlert(clock.instant(), turbineID, err.text))

        alertsErrorQueue.clear()
        events
      }

      // it's first alert after recover
      // it needs to protect from duplicate of notifications
      val eventsAfterCheck = if (recoveryState.lastEventIsAlert.nonEmpty) {
        val error = recoveryState.lastEventIsAlert.get
        updateRecoveryState(recoveryState.nextBehaviour, None)
        if (events.nonEmpty && events.head._1.errorText.equals(error)) events.tail else events
      } else {
        events
      }

      eventsAfterCheck.foreach(e => {
        alertActor ! e._2

        // if persist message is failed then two notification will be sent.
        // One of messages from code line below and other from recovery function
        // it is more safely then firstly persist message and then send alert
        persist(e._1)(_ => {})
      })

      msg match {
        case NeedSendAlerts =>
        case e => persist(e) { e =>
          updateState(e)

          val nextBehaviour = behaviour.apply(msg)
          // goto next behaviour
          context.become(processingBehaviour(nextBehaviour))

          // I make snapshot only on working state, otherwise it is not easy to restore state and satisfy property:
          // "it must not miss or duplicate any notifications due to crash/restart"
          nextBehaviour match {
            case WorkingBehaviour() if needToMakeSnapshot(lastSequenceNr) =>
              lastSnapshotSequenceNr = lastSequenceNr
              saveSnapshot(state)

            case _ =>
          }

          cSender ! Reply
        }
      }
    }

    def isDefinedAt(x: Any): Boolean = true
  }

  def receiveCommand: Receive = processingBehaviour(WorkingBehaviour())

  protected def stopTimer(): Unit = {
    cancellable match {
      case Some(c) =>
        c.cancel()
        cancellable = None
      case None =>
    }
  }

  protected def startTimer(error: TurbineError, timeout: FiniteDuration): Unit = {
    stopTimer()

    cancellable = Some(context.system.scheduler.scheduleOnce(durationConverter.convert(timeout))({
      sendAlert(error)
    }))
  }

  def receiveRecover: Receive = {
    case RecoveryCompleted =>
      synchronized {
        alertsErrorQueue.clear()
      }
      context.become(processingBehaviour(recoveryState.nextBehaviour))
    case SnapshotOffer(_, snapshot: State) =>
      state = snapshot
      updateRecoveryState(WorkingBehaviour(), None)
    case SendAlertEvent(err) =>
      updateRecoveryState(recoveryState.nextBehaviour, Some(err))
    case e =>
      updateState(e)
      updateRecoveryState(recoveryState.nextBehaviour.apply(e), None)
  }
}