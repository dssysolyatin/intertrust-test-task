package com.intertrust.actors

import java.time.{Clock, Instant}
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, Props, Scheduler}
import akka.pattern.{BackoffOpts, BackoffSupervisor}
import akka.util.Timeout
import com.intertrust.actors.ControllerActor.State
import com.intertrust.utils.backoffRetry
import com.intertrust.protocol._
import com.intertrust.time.FiniteDurationConverter

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.control.NonFatal


object ControllerActor {
  type ActorMap = Map[String, (ActorRef, Future[Any])]

  // Need store Futures in ControllerState in order to sent delayed message in the same order which they came,
  // otherwise unexpected situation can be appear
  // For example: Controller receive two TurbineStatus events: (Broken, Working) and TurbineActor is failed when
  // controller tried to send `Broken` message. `Working` message can be sent successfully while `Broken` is waiting retry.
  // As result, TurbineActor receive (Working, Broken) instead of (Broken, Working)
  case class State(technicians: ActorMap, turbines: ActorMap) {
    def addTechnician(id: String, ref: ActorRef): State = {
      copy(technicians + (id -> (ref, Future.successful())), turbines)
    }

    def addTurbine(id: String, ref: ActorRef): State = {
      copy(technicians, turbines + (id -> (ref, Future.successful())))
    }

    def appendFutureToTechnician[S](id: String, future: => Future[S])(implicit ec: ExecutionContext): State = {
      copy(technicians + (id -> mixFuture(technicians)(id, future)), turbines)
    }

    def appendFutureToTurbine[S](id: String, future: => Future[S])(implicit ec: ExecutionContext): State = {
      copy(technicians, turbines + (id -> mixFuture(turbines)(id, future)))
    }

    def mixFuture[S](map: ActorMap)(id: String, future: => Future[S])(implicit ec: ExecutionContext): (ActorRef, Future[S]) = {
      val entity = map(id)
      (entity._1, entity._2 flatMap (_ => future))
    }

    def getTechnician(id: String): Option[ActorRef] = {
      getRef(technicians)(id)
    }

    def getTurbine(id: String): Option[ActorRef] = {
      getRef(turbines)(id)
    }

    def getRef(map: ActorMap)(id: String): Option[ActorRef] = {
      map.get(id) match {
        case Some((ref, _)) => Some(ref)
        case None => None
      }
    }
  }
}

class ControllerActor(durationConverter: FiniteDurationConverter, clock: Clock, alertActor: ActorRef)
  extends Actor {

  import akka.pattern.{after, ask, pipe}

  implicit val executeContext: ExecutionContext = context.system.dispatcher
  implicit val scheduler: Scheduler = context.system.scheduler

  var state: State = State(Map.empty, Map.empty)

  def receive: Receive = {
    case e: TurbineEvent =>
      sendMessageToTurbine(e.turbineId, e.status, e.timestamp, sender())
    case e: MovementEvent =>
      e.location match {
        case Turbine(turbineId) =>
          sendToTechnicianAndTurbine(e.engineerId, turbineId, e, e.timestamp, sender())
        case _ =>
          sendMessageToTechnician(e.engineerId, TechnicianMovement(e.location, e.movement), e.timestamp, sender())
      }
  }

  protected def getTurbineRef(id: String): ActorRef = {
    state.getTurbine(id) match {
      case Some(actor) => actor
      case None =>
        val ref = createTurbineRef(id)
        state = state.addTurbine(id, ref)
        ref
    }
  }

  protected def createTurbineRef(id: String): ActorRef = {
    createBackoffSupervisor(
      Props(classOf[TurbineActor], id, durationConverter, clock, alertActor),
      s"turbine-$id"
    )
  }

  protected def getTechnicianRef(id: String): ActorRef = {
    state.getTechnician(id) match {
      case Some(actor) => actor
      case None =>
        val ref = createTechnicianRef(id)
        state = state.addTechnician(id, ref)
        ref
    }
  }

  protected def createTechnicianRef(id: String): ActorRef = {
    createBackoffSupervisor(
      Props(classOf[TechnicianActor], id, clock, alertActor),
      s"technician-${id}"
    )
  }

  protected def sendToTechnicianAndTurbine(
                                            technicianId: String,
                                            turbineId: String,
                                            e: MovementEvent,
                                            timeToSend: Instant,
                                            sender: ActorRef
                                          ): Unit = {

    implicit val timeout: Timeout = Timeout(5.seconds)
    val technicianRef = getTechnicianRef(technicianId)
    val turbineRef = getTurbineRef(turbineId)

    // At the moment, movement message will be send to TurbineActor only after TechnicianActor has received this message and has sent reply
    // This is done so that we can add custom logic.
    // For example: don't send message to TurbineActor if TechnicalActor returns BadMovementReply
    lazy val sendMessageToTechnicianFuture = createBackoffForFuture(
      () => ask(technicianRef, TechnicianMovement(e.location, e.movement))
    )
    lazy val delayedSendMessageToTechnicianFuture = delayedAction(sendMessageToTechnicianFuture, timeToSend)
    appendFutureToTechnician(technicianId, delayedSendMessageToTechnicianFuture)

    lazy val sendMessageToTurbineFuture = createBackoffForFuture(
      () => ask(turbineRef, TurbineTechnicianMovement(e.engineerId, e.movement))
    )

    lazy val future = delayedSendMessageToTechnicianFuture flatMap (_ => sendMessageToTurbineFuture) map (_ => Reply) pipeTo sender
    appendFutureToTurbine(turbineId, future)
  }


  protected def appendFutureToTechnician[T](id: String, future: => Future[T]): Unit = {
    state = state.appendFutureToTechnician(id, future)
  }

  protected def appendFutureToTurbine[T](id: String, future: => Future[T]): Unit = {
    state = state.appendFutureToTurbine(id, future)
  }

  protected def sendMessage(receiverGetter: (String) => ActorRef, futureAppender: (String, => Future[Reply]) => Unit)
                           (id: String, msg: Any, timeToSend: Instant, sender: ActorRef): Unit = {
    implicit val timeout: Timeout = Timeout(5.seconds)

    val ref = receiverGetter(id)
    lazy val sendMessage = createBackoffForFuture(() => ask(ref, msg))
    futureAppender(id, delayedAction(sendMessage.map(_ => Reply).pipeTo(sender), timeToSend))
  }

  val sendMessageToTurbine = sendMessage(getTurbineRef, appendFutureToTurbine) _

  val sendMessageToTechnician = sendMessage(getTechnicianRef, appendFutureToTechnician) _

  protected def delayedAction[T](future: => Future[T], timeToSend: Instant): Future[T] = {
    val currentTime = clock.instant()
    if (currentTime.compareTo(timeToSend) >= 0) {
      try future catch {
        case NonFatal(t) ⇒ Future.failed(t)
      }
    } else {
      val duration = durationConverter.convert(FiniteDuration(timeToSend.toEpochMilli - currentTime.toEpochMilli, TimeUnit.MILLISECONDS))
      after(duration, context.system.scheduler)(future)
    }
  }

  protected def createBackoffForFuture[T](futureFactory: () => Future[T]): Future[T] = {
    backoffRetry(1.seconds, 1.minutes, 0.2)(futureFactory)
  }

  protected def createBackoffSupervisor(childProps: Props, childName: String): ActorRef = {
    val supervisor = BackoffSupervisor.props(
      BackoffOpts.onStop(
        childProps = childProps,
        childName = childName,
        minBackoff = 1.seconds,
        maxBackoff = 1.minutes,
        randomFactor = 0.2
      )
    )

    context.actorOf(supervisor, s"supervisor-$childName")
  }
}
