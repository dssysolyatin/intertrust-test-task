package com.intertrust.time

import java.time.{Clock, Instant, ZoneId}

class LinearClock(realTimeClock: Clock, initialTime: Instant, speedX: Double) extends Clock {
  private val realStartingPoint = realTimeClock.instant()

  def getZone: ZoneId = {
    realTimeClock.getZone
  }

  def withZone(zone: ZoneId): Clock = {
    new LinearClock(realTimeClock.withZone(zone), initialTime, speedX)
  }

  def instant(): Instant = {
    val realTimeDuration = (realTimeClock.instant().toEpochMilli - realStartingPoint.toEpochMilli) * speedX
    Instant.ofEpochMilli(initialTime.toEpochMilli + realTimeDuration.toLong)
  }
}
