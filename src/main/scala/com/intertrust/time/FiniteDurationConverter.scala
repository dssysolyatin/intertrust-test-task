package com.intertrust.time

import scala.concurrent.duration.FiniteDuration

trait FiniteDurationConverter {
  def convert(duration: FiniteDuration): FiniteDuration
}


class LinearDurationConverter(timeCoefficient: Double) extends FiniteDurationConverter {
  def convert(duration: FiniteDuration): FiniteDuration = {
    duration * timeCoefficient match {
      case t: FiniteDuration => t
      case _ => throw new RuntimeException("invalid timeCoefficient factor")
    }
  }
}