package com.intertrust.behaviour

import com.intertrust.protocol.Reply

abstract class Behaviour extends (Any => Behaviour) {}

abstract class ReplyBehaviour extends Behaviour {
  def apply(v1: Any): Behaviour = applyWithReply(v1)._1

  def applyWithReply(msg: Any): (Behaviour, Reply)
}