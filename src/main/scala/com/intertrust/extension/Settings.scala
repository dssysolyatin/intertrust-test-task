package com.intertrust.extension

import akka.actor.{ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider}
import com.typesafe.config.{Config, ConfigException}

class SettingsImpl(config: Config) extends Extension {
  val speedUp: Double = try config.getDouble("simulator.speedup") catch {
    case _: ConfigException.Missing => 1d
  }

  // This file to store how many lines have been read from events files
  val queyerStateFile: String = config.getString("simulator.queyer.statefile")
}

object Settings extends ExtensionId[SettingsImpl] with ExtensionIdProvider {
  def createExtension(system: ExtendedActorSystem): SettingsImpl = {
    new SettingsImpl(system.settings.config)
  }

  def lookup(): ExtensionId[SettingsImpl] = Settings
}
