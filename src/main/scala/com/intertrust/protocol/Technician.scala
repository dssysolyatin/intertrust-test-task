package com.intertrust.protocol

case class TechnicianMovement(location: Location, movement: Movement)

sealed trait TechnicianMovementReply extends Reply

case object BadMovementReply extends TechnicianMovementReply

case object GoodMovementReply extends TechnicianMovementReply
