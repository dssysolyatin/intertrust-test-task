package com.intertrust.protocol

case class TurbineTechnicianMovement(technicianID: String, movement: Movement)
