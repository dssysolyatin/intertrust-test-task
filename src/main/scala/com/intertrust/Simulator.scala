package com.intertrust

import java.time.Clock

import akka.Done
import akka.actor.{ActorSystem, Props}
import akka.routing.FromConfig
import akka.stream.{ActorMaterializer, scaladsl}
import akka.stream.scaladsl.Sink
import akka.util.Timeout
import better.files.File
import com.intertrust.actors.{AlertsActor, ControllerActor}
import com.intertrust.extension.Settings
import com.intertrust.parsers.{MovementEventParser, TurbineEventParser}
import com.intertrust.protocol.{Reply, TimeEvent}
import com.intertrust.utils.MergeIterator
import better.files.Dsl.SymbolicOperations
import com.intertrust.time.{LinearClock, LinearDurationConverter}

import scala.concurrent.{Await, Future}
import scala.io.Source
import scala.concurrent.duration._


object Simulator {
  def main(args: Array[String]): Unit = {
    val movementEvents = new MovementEventParser().parseEvents(Source.fromInputStream(getClass.getResourceAsStream("/movements.csv")))
    val turbineEvents = new TurbineEventParser().parseEvents(Source.fromInputStream(getClass.getResourceAsStream("/turbines.csv")))
    val mergeIterator = new MergeIterator[TimeEvent](Array(movementEvents, turbineEvents))((x: TimeEvent, y: TimeEvent) => {
      x.timestamp.compareTo(y.timestamp)
    })

    if (!mergeIterator.hasNext) {
      println("Events files are empty")
      return
    }

    implicit val system = ActorSystem("simulator")
    implicit val materializer = ActorMaterializer()

    val settings = Settings(system)

    val queyerStateFile = File(settings.queyerStateFile)
      .createFileIfNotExists(createParents = true)
    println(s"queyer state file: ${queyerStateFile.path}")

    val speedUp = settings.speedUp
    val durationConverter = new LinearDurationConverter(1d / speedUp)
    val startPoint = mergeIterator.next()
    // return value back
    val source = scaladsl.Source.fromIterator(() => Array(startPoint).toIterator ++ mergeIterator)

    // creating actors
    val alertsActor = system.actorOf(FromConfig.props(Props[AlertsActor]), "alerts")
    val controllerActor = system.actorOf(Props(
      new ControllerActor(
        durationConverter,
        new LinearClock(Clock.systemUTC(), startPoint.timestamp, speedUp),
        alertsActor,
      )), "controller")


    val numberSkipEvents = queyerStateFile.contentAsString match {
      case "" => 0
      case content => content.toLong
    }

    implicit val timeout = Timeout(5.minutes + durationConverter.convert(1.day))
    val result: Future[Done] = source
      .drop(numberSkipEvents)
      .ask[Reply](parallelism = 10)(controllerActor)
      .fold(numberSkipEvents)((i, _) => {
        queyerStateFile < (i + 1).toString
        i + 1
      })
      .runWith(Sink.ignore)

    Await.result(result, 365.days)
    system.terminate()
  }
}
