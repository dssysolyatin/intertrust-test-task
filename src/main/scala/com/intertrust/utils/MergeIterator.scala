package com.intertrust.utils

class MergeIterator[A](iterators: Array[Iterator[A]])(implicit ordering: Ordering[A]) extends Iterator[A] {

  // Store id in order to not to call zipWithIndex every time
  protected val values = new Array[(Option[A], Int)](iterators.length)

  for ((i, idx) <- iterators.zipWithIndex) {
    if (i.hasNext) {
      values(idx) = (Some(i.next()), idx)
    } else {
      values(idx) = (None, idx)
    }
  }

  private val valuesOrdering = new Ordering[(Option[A], Int)] {
    def compare(x: (Option[A], Int), y: (Option[A], Int)): Int = {
      // in this it always some
      ((x._1, y._1): @unchecked) match {
        case (Some(xVal), Some(yVal)) => ordering.compare(xVal, yVal)
      }
    }
  }

  def hasNext: Boolean = values.exists(_._1.isDefined)

  def next(): A = {
    val (minVal, minIdx) = values.filter(_._1.isDefined).min(valuesOrdering)
    val iter = iterators(minIdx)

    if (iter.hasNext) {
      values(minIdx) = (Some(iter.next()), minIdx)
    } else {
      values(minIdx) = (None, minIdx)
    }

    // in this case minVal always exists
    (minVal: @unchecked) match {
      case Some(x) => x
    }
  }
}
