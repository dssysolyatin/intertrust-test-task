package com.intertrust.utils

import akka.actor.Scheduler

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.FiniteDuration
import akka.pattern.after

import scala.util.control.NonFatal

// I didn't find normal library for this purpose
trait BackoffRetry {
  def backoffRetry[T](minBackoff: FiniteDuration, maxBackoff: FiniteDuration, randomFactor: Double)(attempt: => () => Future[T])(
    implicit ec: ExecutionContext, scheduler: Scheduler): Future[T] = {

    def retry(calls: Int): Future[T] = {
      val nextDelayWithoutFactor = minBackoff * (1L << calls)
      val nextDelay = nextDelayWithoutFactor + nextDelayWithoutFactor * randomFactor match {
        case t: FiniteDuration => t
        case _ => throw new RuntimeException("invalid randomFactor")
      }

      try {
        if (nextDelay < maxBackoff) {
          attempt().recoverWith {
            case NonFatal(_) =>
              after(nextDelay, scheduler) {
                retry(calls + 1)
              }
          }
        } else {
          attempt()
        }
      } catch {
        case NonFatal(error) => Future.failed(error)
      }
    }

    retry(0)
  }
}

