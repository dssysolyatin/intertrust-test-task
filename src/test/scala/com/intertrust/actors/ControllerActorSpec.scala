package com.intertrust.actors

import java.text.SimpleDateFormat
import java.time.Clock
import java.util.TimeZone
import java.util.UUID.randomUUID

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.intertrust.protocol.Movement.Enter
import com.intertrust.protocol.TurbineStatus.{Broken, Working}
import com.intertrust.protocol._
import com.intertrust.time.{FiniteDurationConverter, LinearClock, LinearDurationConverter}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

import scala.concurrent.duration._


class ProxyReplyActor(res: ActorRef) extends Actor {
  def receive: Receive = {
    case e: TechnicianMovement =>
      res ! e
      sender() ! GoodMovementReply
    case e =>
      res ! e
      sender() ! Reply
  }
}

class ControllerActorTest(
                           durationConverter: FiniteDurationConverter,
                           clock: Clock,
                           alertActor: ActorRef,
                           techniciansActors: Map[String, ActorRef],
                           turbineActors: Map[String, ActorRef]
                         ) extends ControllerActor(durationConverter, clock, alertActor) {

  override def createTechnicianRef(id: String): ActorRef = {
    techniciansActors(id)
  }

  override def createTurbineRef(id: String): ActorRef = {
    turbineActors(id)
  }
}

class ControllerActorSpec extends TestKit(ActorSystem("ControllerActorSpec"))
  with WordSpecLike with BeforeAndAfterAll with ImplicitSender {

  class ProbeMap {
    var mapRef = Map.empty[String, ActorRef]
    var mapProbe = Map.empty[String, TestProbe]

    def add(id: String): Unit = {
      val probe = TestProbe()
      mapRef += id -> system.actorOf(Props(new ProxyReplyActor(probe.ref)))
      mapProbe += id -> probe
    }

    def probe(id: String): TestProbe = {
      (mapProbe.get(id): @unchecked) match {
        case Some(x) => x
      }
    }
  }

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val speedX = 10
  val formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm")
  formatter.setTimeZone(TimeZone.getTimeZone("UTC"))

  val clock = new LinearClock(Clock.systemUTC(), formatter.parse("23.11.2015 06:37").toInstant, speedX)

  def createActor(): (ActorRef, ProbeMap, ProbeMap) = {
    val technicians = new ProbeMap
    technicians.add("P1")

    val turbines = new ProbeMap
    turbines.add("B4B")

    val controllerActor = system.actorOf(Props(
      new ControllerActorTest(
        new LinearDurationConverter(1d / speedX),
        clock,
        TestProbe().ref,
        technicians.mapRef,
        turbines.mapRef
      )), "controller" + randomUUID().toString)

    (controllerActor, technicians, turbines)
  }


  "A Controller actor" must {
    "send movement event to a technician actor" in {
      val (actor, technicians, _) = createActor()
      actor ! MovementEvent("P1", Vessel("235098384"), Enter, formatter.parse("23.11.2015 06:37").toInstant)
      val msg = technicians.probe("P1").expectMsgType[TechnicianMovement]
      assert(msg.equals(TechnicianMovement(Vessel("235098384"), Enter)))
      expectMsgType[Reply]
    }

    "send movement event to a technician actor and to turbine actor if location is turbine" in {
      val (actor, technicians, turbines) = createActor()

      actor ! MovementEvent("P1", Turbine("B4B"), Enter, formatter.parse("23.11.2015 06:37").toInstant)
      val technician = technicians.probe("P1")
      val msg = technician.expectMsgType[TechnicianMovement]
      assert(msg.equals(TechnicianMovement(Turbine("B4B"), Enter)))

      val movement = turbines.probe("B4B").expectMsgType[TurbineTechnicianMovement]
      assert(movement.technicianID == "P1")
      assert(movement.movement.equals(Enter))
      expectMsgType[Reply]
    }

    "send turbine event to a turbine actor" in {
      val (actor, _, turbines) = createActor()

      actor ! TurbineEvent("B4B", Working, 1.21, formatter.parse("23.11.2015 06:37").toInstant)
      val msg = turbines.probe("B4B").expectMsgType[TurbineStatus]
      assert(msg.equals(Working))
      expectMsgType[Reply]
    }

    "correct send delayed message on turbine events" in {
      val (actor, _, turbines) = createActor()
      val currentTime = clock.instant()

      // the messages should processing in the order in which they came
      actor ! TurbineEvent("B4B", Broken, 1.21, currentTime.plusSeconds(20))
      actor ! TurbineEvent("B4B", Working, 1.21, currentTime.plusSeconds(10))
      actor ! TurbineEvent("B4B", Broken, 1.21, currentTime.plusSeconds(60))

      within((18 / speedX).seconds, (30 / speedX).seconds) {
        val msg = turbines.probe("B4B").expectMsgType[TurbineStatus]
        assert(msg.equals(Broken))
      }
      expectMsgType[Reply]

      val msg = turbines.probe("B4B").expectMsgType[TurbineStatus]
      assert(msg.equals(Working))
      expectMsgType[Reply]

      within((38 / speedX).seconds, (50 / speedX).seconds) {
        val msg = turbines.probe("B4B").expectMsgType[TurbineStatus]((50 / speedX).seconds)
        assert(msg.equals(Broken))
      }
      expectMsgType[Reply]
    }

    "correct send delayed message on technician events" in {
      val (actor, technicians, turbines) = createActor()
      val currentTime = clock.instant()

      // the messages should processing in the order in which they came
      actor ! MovementEvent("P1", Vessel("235098384"), Enter, currentTime.plusSeconds(20))
      actor ! MovementEvent("P1", Vessel("235098385"), Enter, currentTime.plusSeconds(10))
      actor ! MovementEvent("P1", Turbine("B4B"), Enter, currentTime.plusSeconds(60))

      within((18 / speedX).seconds, (30 / speedX).seconds) {
        val msg = technicians.probe("P1").expectMsgType[TechnicianMovement]
        assert(msg.equals(TechnicianMovement(Vessel("235098384"), Enter)))
      }
      expectMsgType[Reply]

      {
        val msg = technicians.probe("P1").expectMsgType[TechnicianMovement]
        assert(msg.equals(TechnicianMovement(Vessel("235098385"), Enter)))
        expectMsgType[Reply]
      }

      val maxDuration = (50 / speedX).seconds
      within((38 / speedX).seconds, maxDuration) {
        val msg = technicians.probe("P1").expectMsgType[TechnicianMovement](maxDuration)
        assert(msg.equals(TechnicianMovement(Turbine("B4B"), Enter)))

        val msg2 = turbines.probe("B4B").expectMsgType[TurbineTechnicianMovement](maxDuration)
        assert(msg2.equals(TurbineTechnicianMovement("P1", Enter)))
      }
      // check that two messages are not sent to turbine actor. It's for checking combine future logic
      technicians.probe("P1").expectNoMessage()
      expectMsgType[Reply]
    }
  }
}
