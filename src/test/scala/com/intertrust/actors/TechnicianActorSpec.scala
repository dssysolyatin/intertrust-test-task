package com.intertrust.actors

import java.time.Clock
import java.util.UUID.randomUUID

import akka.actor.{ActorRef, ActorSystem, PoisonPill, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.intertrust.protocol.Movement.{Enter, Exit}
import com.intertrust.protocol._
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

class TechnicianActorTest(technicianID: String, clock: Clock, alertActor: ActorRef)
  extends TechnicianActor(technicianID, clock, alertActor) {
  override def snapshotInterval: Int = 3
}

class TechnicianActorSpec
  extends TestKit(ActorSystem("TechnicianActorSpec"))
    with WordSpecLike with ImplicitSender with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  def createActor(): (ActorRef, TestProbe) = createActorWithId(randomUUID().toString)

  def createActorWithId(id: String): (ActorRef, TestProbe) = {
    val probe = TestProbe()
    val technicianActor = system.actorOf(Props(
      new TechnicianActorTest(id, Clock.systemUTC(), probe.ref))
    )

    (technicianActor, probe)
  }

  def expectAlert(actor: ActorRef, probe: TestProbe, location: Location, movement: Movement): Unit = {
    actor ! TechnicianMovement(location, movement)
    val reply = expectMsgType[TechnicianMovementReply]
    assert(reply.equals(BadMovementReply))
    probe.expectMsgType[MovementAlert]
  }

  def expectGoodReply(actor: ActorRef, location: Location, movement: Movement) = {
    actor ! TechnicianMovement(location, movement)
    val reply = expectMsgType[TechnicianMovementReply]
    assert(reply.equals(GoodMovementReply))
  }


  "A Technician actor" must {
    "send alert" when {
      "the technician entered to the vessel and then technician enter to the turbine" in {
        val (actor, probe) = createActor()

        expectGoodReply(actor, Vessel("1"), Enter)
        expectAlert(actor, probe, Turbine("1"), Enter)
      }

      "the technician entered to the turbine and then technician enter to another turbine" in {
        val (actor, probe) = createActor()

        expectGoodReply(actor, Turbine("1"), Enter)
        expectAlert(actor, probe, Turbine("2"), Enter)
      }

      "the technician entered to the turbine and then technician enter to the vessel" in {
        val (actor, probe) = createActor()

        expectGoodReply(actor, Turbine("1"), Enter)
        expectAlert(actor, probe, Vessel("1"), Enter)
      }

      "the technician entered to the vessel and then technician enter to another vessel" in {
        val (actor, probe) = createActor()

        expectGoodReply(actor, Vessel("1"), Enter)
        expectAlert(actor, probe, Vessel("2"), Enter)
      }

      "the technician didn't enter to the vessel and then technician exit from the vessel" in {
        val (actor, probe) = createActor()
        expectAlert(actor, probe, Vessel("2"), Exit)
      }

      "the technician didn't enter to the turbine and then technician exit from the turbine" in {
        val (actor, probe) = createActor()
        expectAlert(actor, probe, Turbine("2"), Exit)
      }
    }

    "not send alert" when {
      "the technician entered to the vessel and then technician exit from the vessel" in {
        val (actor, _) = createActor()

        expectGoodReply(actor, Vessel("1"), Enter)
        expectGoodReply(actor, Vessel("1"), Exit)
      }

      "the technician entered to the turbine and then technician exit from the turbine" in {
        val (actor, _) = createActor()

        expectGoodReply(actor, Turbine("2"), Enter)
        expectGoodReply(actor, Turbine("2"), Exit)
      }

      "the technician entered to the turbine, exit from the turbine, enter to the vessel, exit from the vessel" in {
        val (actor, _) = createActor()

        expectGoodReply(actor, Turbine("1"), Enter)
        expectGoodReply(actor, Turbine("1"), Exit)
        expectGoodReply(actor, Vessel("1"), Enter)
        expectGoodReply(actor, Vessel("1"), Exit)
      }

      "double send the same event" in {
        val (actor, _) = createActor()

        expectGoodReply(actor, Turbine("1"), Enter)
        expectGoodReply(actor, Turbine("1"), Enter)

        expectGoodReply(actor, Turbine("1"), Exit)
        expectGoodReply(actor, Turbine("1"), Exit)

        expectGoodReply(actor, Vessel("1"), Enter)
        expectGoodReply(actor, Vessel("1"), Enter)

        expectGoodReply(actor, Vessel("1"), Exit)
        expectGoodReply(actor, Vessel("1"), Exit)
      }
    }

    "restore correctly after fail" when {
      "there was one event = technician entered to the turbine" in {
        val id = randomUUID().toString
        val (actor, _) = createActorWithId(id)

        expectGoodReply(actor, Turbine("2"), Enter)
        actor ! PoisonPill

        val (restoredActor, probe) = createActorWithId(id)
        expectAlert(restoredActor, probe, Turbine("1"), Enter)
      }

      "sequence of actions: technician enter in turbine, exit from turbine, enter to vessel, exit from vessel" in {
        val id = randomUUID().toString
        val (actor, _) = createActorWithId(id)

        expectGoodReply(actor, Turbine("2"), Enter)
        expectGoodReply(actor, Turbine("2"), Exit)
        expectGoodReply(actor, Vessel("1"), Enter)
        expectGoodReply(actor, Vessel("1"), Exit)

        actor ! PoisonPill

        val (restoredActor, probe) = createActorWithId(id)
        expectAlert(restoredActor, probe, Turbine("1"), Exit)
      }
    }
  }
}