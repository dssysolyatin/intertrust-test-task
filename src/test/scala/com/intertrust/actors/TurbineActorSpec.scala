package com.intertrust.actors

import java.time.Clock
import java.util.UUID.randomUUID

import akka.actor.{ActorRef, ActorSystem, PoisonPill, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.intertrust.protocol.Movement.{Enter, Exit}
import com.intertrust.protocol.{Reply, TurbineAlert, TurbineTechnicianMovement}
import com.intertrust.protocol.TurbineStatus.{Broken, Working}
import com.intertrust.time.{FiniteDurationConverter, LinearClock, LinearDurationConverter}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

import scala.concurrent.duration._


class TurbineActorTest(turbineID: String, durationConverter: FiniteDurationConverter, clock: Clock, alertActor: ActorRef)
  extends TurbineActor(turbineID, durationConverter, clock, alertActor) {
}

class TurbineActorSpec
  extends TestKit(ActorSystem("TurbineActorSpec"))
    with ImplicitSender with WordSpecLike with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val speedX = 10000
  val clock = new LinearClock(Clock.systemUTC(), Clock.systemUTC().instant(), speedX)
  val durationConverter = new LinearDurationConverter(1d / speedX)

  val (technicianId1, technicianId2) = ("P12", "P13")

  def createActorWithId(id: String): (ActorRef, TestProbe) = {
    val probe = TestProbe()
    val actor = system.actorOf(
      Props(new TurbineActor(id, durationConverter, clock, probe.ref))
    )

    (actor, probe)
  }

  def createActor: (ActorRef, TestProbe) = createActorWithId(randomUUID().toString)

  def createTurbineActorAndMoveToBrokenState(): (ActorRef, TestProbe) = {
    val (actorRef, probe) = createActor
    send(actorRef, Broken)
    expectedBrokenAlert(probe)

    (actorRef, probe)
  }

  def timeConvert: FiniteDuration => FiniteDuration = durationConverter.convert

  def expectedBrokenAlert(probe: TestProbe): Unit = {
    val alert = probe.expectMsgType[TurbineAlert]
    assert(alert.error == TurbineError.TurbineStopWorking.text)
  }

  def expectAlertWithIn(probe: TestProbe, error: TurbineError, min: FiniteDuration, max: FiniteDuration): Unit = {
    within(timeConvert(min), timeConvert(max) + 100.milliseconds) {
      val alert = probe.expectMsgType[TurbineAlert](timeConvert(max) + 100.milliseconds)
      assert(alert.error == error.text)
    }
  }

  def send(actor: ActorRef, msg: Any): Unit = {
    actor ! msg
    expectMsgType[Reply]
  }


  "A Turbine actor" must {
    "send alert" when {
      "the turbine is broken" in {
        createTurbineActorAndMoveToBrokenState()
      }

      "the turbine is broken more than 4 hour" in {
        val (_, probe) = createTurbineActorAndMoveToBrokenState()

        expectAlertWithIn(probe, TurbineError.TurbineStillBrokenAfter4Hour, 4.hour - 5.minutes, 4.hour + 5.minutes)
      }

      "the turbine continues in a Broken state for more than 3 minutes after a technician has exited the turbine" in {
        val (actor, probe) = createTurbineActorAndMoveToBrokenState()

        send(actor, TurbineTechnicianMovement(technicianId1, Enter))
        send(actor, TurbineTechnicianMovement(technicianId1, Exit))

        expectAlertWithIn(probe, TurbineError.TurbineStillBrokenAfterRepair, 2.minutes, 4.minutes)
      }
    }

    "not send alert" when {
      "the turbine was broken less than 4 hour ago and a technician has entered to the turbine" in {
        val (actor, probe) = createTurbineActorAndMoveToBrokenState()

        probe.expectNoMessage(timeConvert(3.hour + 50.minutes))
        send(actor, TurbineTechnicianMovement(technicianId1, Enter))
        probe.expectNoMessage(timeConvert(4.hour + 10.minutes))
      }

      "a technician repaired the turbine and exit from it" in {
        val (actor, probe) = createTurbineActorAndMoveToBrokenState()

        send(actor, TurbineTechnicianMovement(technicianId1, Enter))
        send(actor, Working)
        send(actor, TurbineTechnicianMovement(technicianId1, Exit))
        probe.expectNoMessage(timeConvert(5.minutes))
      }

      "a technician exit from the turbine, but there is one more technician in the turbine" in {
        val (actor, probe) = createTurbineActorAndMoveToBrokenState()
        send(actor, TurbineTechnicianMovement(technicianId1, Enter))
        send(actor, TurbineTechnicianMovement(technicianId2, Enter))

        send(actor, TurbineTechnicianMovement(technicianId1, Exit))
        probe.expectNoMessage(timeConvert(10.minutes))
      }
    }

    "restore correctly after fail" when {
      "there was one event = turbine was broken, message about breakdown was sent" in {
        val id = randomUUID().toString
        val (actor, probe) = createActorWithId(id)

        send(actor, Broken)
        expectedBrokenAlert(probe)
        expectAlertWithIn(probe, TurbineError.TurbineStillBrokenAfter4Hour, 4.hour - 5.minutes, 4.hour + 5.minutes)
        actor ! PoisonPill

        val (_, restoredProbe) = createActorWithId(id)
        restoredProbe.expectNoMessage(timeConvert(4.hour + 5.minutes))
      }

      "there was one event = turbine was broken, message about breakdown was not sent" in {
        val id = randomUUID().toString
        val (actor, probe) = createActorWithId(id)

        send(actor, Broken)
        expectedBrokenAlert(probe)
        actor ! PoisonPill

        val (_, restoredProbe) = createActorWithId(id)
        expectAlertWithIn(restoredProbe, TurbineError.TurbineStillBrokenAfter4Hour, 4.hour - 5.minutes, 4.hour + 5.minutes)
        restoredProbe.expectNoMessage(timeConvert(4.hour + 5.minutes))
      }

      "sequence of actions: technician enter in turbine, exit from turbine, enter to vessel, exit from vessel" in {
        val id = randomUUID().toString
        val (actor, _) = createActorWithId(id)

        send(actor, Broken)
        send(actor, TurbineTechnicianMovement(technicianId1, Enter))
        send(actor, TurbineTechnicianMovement(technicianId2, Enter))
        send(actor, TurbineTechnicianMovement(technicianId2, Exit))

        actor ! PoisonPill

        val (restoredActor, probe) = createActorWithId(id)
        send(restoredActor, TurbineTechnicianMovement(technicianId1, Exit))
        expectAlertWithIn(probe, TurbineError.TurbineStillBrokenAfterRepair, 2.minutes, 4.minutes)
      }
    }
  }
}