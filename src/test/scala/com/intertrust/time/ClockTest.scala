package com.intertrust.time

import java.text.SimpleDateFormat
import java.time.{Clock, Instant, ZoneId}
import java.util.TimeZone

import org.scalatest.FunSuite

class NextDateClock(initialDate: Instant) extends Clock {
  var calls: Int = 0

  override def getZone: ZoneId = ZoneId.systemDefault()

  override def withZone(zone: ZoneId): Clock = Clock.system(zone)

  override def instant(): Instant = {
    calls += 1
    initialDate.plusMillis(86400000 * calls)
  }
}

class ClockTest extends FunSuite {
  test("Clock.instant") {
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"))

    val clock = new LinearClock(
      new NextDateClock(formatter.parse("2015-11-23").toInstant),
      formatter.parse("2019-06-28").toInstant,
      10
    )

    assert(clock.instant() == formatter.parse("2019-07-08").toInstant)
    assert(clock.instant() == formatter.parse("2019-07-18").toInstant)
  }
}
