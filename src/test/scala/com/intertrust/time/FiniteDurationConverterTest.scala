package com.intertrust.time

import org.scalatest.FunSuite

import scala.concurrent.duration._

class FiniteDurationConverterTest extends FunSuite {
  test("LinearDurationConverter.convert") {
    val converter = new LinearDurationConverter(0.1)
    assert(converter.convert(100 microseconds) == (10 microseconds))
    assert(converter.convert(10 minutes) == (1 minute))
  }
}
