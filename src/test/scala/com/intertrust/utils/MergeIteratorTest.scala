package com.intertrust.utils

import org.scalatest.FunSuite

class MergeIteratorTest extends FunSuite {
  test("MergeIterator") {
    val iterators = Array((1 to 9 by 2).toIterator, (2 to 10 by 2).toIterator)
    val mergeList = new MergeIterator[Int](iterators).toList

    assert(mergeList == (1 to 10).toList)
  }
}
