package com.intertrust.utils

import java.util.concurrent.atomic.AtomicInteger

import com.miguno.akka.testing.VirtualTime
import org.scalatest.FunSuite

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.Future

class BackoffRetryTest extends FunSuite {


  test("BackoffRetry") {
    val time = new VirtualTime
    val counter = new AtomicInteger()
    implicit val scheduler = time.scheduler

    @tailrec
    def waitCounterIsChanged(maxDuration: Long)(newVal: Int): Boolean = {
      Thread.sleep(50)

      if (maxDuration < 0) {
        false
      } else if (counter.get() == newVal) {
        true
      } else {
        waitCounterIsChanged(maxDuration - 50)(newVal)
      }
    }

    def waitCounterIsChanged100Millis(newVal: Int): Boolean = waitCounterIsChanged(100)(newVal)

    backoffRetry[Nothing](1.second, 9.seconds, 0.2)(() => Future {
      counter.getAndIncrement()

      throw new RuntimeException()
    })

    assert(waitCounterIsChanged100Millis(1))

    // --- new tick --- //
    time.advance(1199 millis)
    assert(!waitCounterIsChanged100Millis(2))

    time.advance(1.millis) // 1200 millis
    assert(waitCounterIsChanged100Millis(2))

    time.advance(2399)
    assert(!waitCounterIsChanged100Millis(3))

    time.advance(1)
    assert(waitCounterIsChanged100Millis(3))

    time.advance(4799)
    assert(!waitCounterIsChanged100Millis(4))

    time.advance(1)
    assert(waitCounterIsChanged100Millis(4))

    time.advance(100000)
    assert(!waitCounterIsChanged100Millis(5))
  }
}
