#!/usr/bin/env sh
# usage POSTGRES_USER=intertrust POSTGRES_DATABASE=intertrust ./restart.sh
psql -U ${POSTGRES_USER} -d ${POSTGRES_DATABASE} -c "TRUNCATE journal; TRUNCATE snapshot; ALTER SEQUENCE journal_ordering_seq RESTART WITH 1; UPDATE journal SET ordering=nextval('journal_ordering_seq');"
rm -rf .queyer_state


