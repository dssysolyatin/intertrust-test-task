name := "backend-test-task"

version := "1.0"

scalaVersion := "2.12.8"

lazy val akkaVersion = "2.5.23"

libraryDependencies ++= Seq(
  "com.beachape" %% "enumeratum" % "1.5.13",
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
  "org.postgresql" % "postgresql" % "42.2.6",
  "com.github.dnvriend" %% "akka-persistence-jdbc" % "3.5.2",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "com.miguno.akka" % "akka-mock-scheduler_2.12" % "0.5.5" % Test,
  "com.github.dnvriend" %% "akka-persistence-inmemory" % "2.5.15.2" % Test,
  "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8",
  "com.github.pathikrit" %% "better-files" % "3.8.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)

fork in Test := true
